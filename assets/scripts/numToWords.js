

const arrayFunction = ["", two_digit, two_digit, three_digit]
const INPUT = document.querySelector("input");
const H3 = document.querySelector("h3");
const ONES = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
const TEENS = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
const TENS = ["", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
const DECIMALS = ["", "hundred", "thousand", "hundred thousand", "million"]

let words = "";
let enteredLength = 0;
// let enteredLength = 0; // number
// let numInput = 0;
let test = ""
let sliceStr = "";
let newNum = 0;

function getInput(){
    const strInput = INPUT.value; //string
    test = strInput; // necessary for slice
    enteredLength = strInput.length; //number
    // numInput = parseFloat(test); //number
    // enteredLength = (enteredLength % 10); //number
    if (isNaN(enteredLength) || isNaN(parseFloat(test))){
        return ;
    } else {
        convertToWords(test, enteredLength)
    }
    console.log(words);
    output()
    words = "";
}

const output = () => {
    return H3.textContent = words;
}



function convertToWords(str, length){
    for (let i = length; i > 0;){
        let iteration = i % 3;
        let j;
        if (iteration == 0){
            j = 3;
        } else if (iteration == 2){
            j = 2;
        } else if (iteration == 1){
            j = 1;
        }

        sliceStr = str.slice(0, j);

        
        for (let k = j; k > 0; k--){
            newNum = parseFloat(sliceStr);
            // console.log(newNum);
            arrayFunction[k](newNum);
            words += " ";
        }
        i = i - j;
        test = test.slice(j);
        console.log(test);
    }
}
function one_digit(num){
    const first = (ONES[num]);
    words += first;
    sliceStr = sliceStr.slice(1); 
}
function two_digit(num){
    // console.log(num);
    let modNum;
    let second;
    // console.log(test.length, "length");
    if(sliceStr.length > 1){
        // console.log("dito?");
        modNum = num % 10;
        if(num >= 10 && num < 20){
            // modNum = num % 10;
            second = TEENS[modNum];
            sliceStr = sliceStr.slice(2);
            words += second;
            // console.log(second, "second");
        }   else if (num >= 20 && num <= 99){
                if(modNum === 0){
                    modNum = parseInt(num/10)
                    second = TENS[modNum];
                    sliceStr = sliceStr.slice(2);
                    words += second;
                } else {
                    modNum = parseInt(num/10)
                    second = TENS[modNum];
                    sliceStr = sliceStr.slice(1);
                    words += second;
                    let newNum = parseFloat(sliceStr);
                    words += " ";
                    one_digit(newNum);
                }
        } else if (num >= 0 && num < 10) {
            if(num = 0){
                sliceStr = sliceStr.slice(2);
            }else {
                sliceStr = sliceStr.slice(1);
            }
        }
    } else {
        if(!sliceStr ){
            return
        } else {
            one_digit(num);
        }
        
    }
    console.log(test.length);
    // return words;
    // console.log(second), "second 3";

    
}

function three_digit(num){
    let place = enteredLength - 2
    modNum = parseInt(num/100)
    if( num % 100 === 0 ){
     // console.log(third);
        sliceStr = sliceStr.slice(3);
    } else {
        sliceStr = sliceStr.slice(1);
    }
    const third = `${ONES[modNum]} ${DECIMALS[place]}`
    words += third;
    // console.log(test)
}

INPUT.addEventListener('change', getInput);